---
title: FAQ
order: 4
---

# FAQ

## Where to put images?

Place the images in the `source/images` folder. Then reference them from markdown as `images/cat.jpg`.

Example:

```md
![Cat yawning](images/cat.jpg 'Cat yawning')
```

## How to set favicon?

Replace file in `source/images/favicon.ico` folder. For further customization use other tools such as [middleman-favicon-maker](https://github.com/follmann/middleman-favicon-maker).
