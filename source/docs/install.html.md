---
title: Installation
order: 1
---

# Installation

## Prerequisites

If you are new to using Middleman or Static Site Generators generally I suggest you to start at [Middleman documentation](//middlemanapp.com/basics/install).

In short you will need Ruby and RubyGems to install Middleman:

```bash
gem install middleman
```

## Init project with a template

To start a project using this template simply run:

```bash
middleman init <MY_PROJECT_FOLDER> -T hajnyon/middleman-uikit-documentation
```

All it essentially does is, that it copies contents of `source` folder (layouts, styles, javascript, content), `data` folder (configurable options) and config files.

After successful initialization you can start to edit the content files or tweak the theme. More about it in [usage section](docs/usage).

## Updating template

The easiest way to update to a new version of template is to initialize it again the same way as in the previous step.

```bash
middleman init <MY_PROJECT_FOLDER> -T hajnyon/middleman-uikit-documentation
```

**CAUTION!** It will overwrite changes you made to certain template files (`data` folder, `layouts`, `partials` etc.) so you need to merge these changes back. In this case Git is your friend and will display changes which you can edit for your liking.

More about updating and versions can be found in this [issue](https://github.com/hajnyon/middleman-uikit-documentation-template/issues/21).
