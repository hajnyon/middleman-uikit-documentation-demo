---
title: Usage
order: 2
---

# Usage

See all options supported by this template and their description.

## Configuration options

### Config.yml

`data/config.yml` file holds configuration options of this template. [Example](https://github.com/hajnyon/middleman-uikit-documentation-template/blob/master/template/data/config.yml)

| property          | data type | default value | example value            | description                                                                                                                    |
| ----------------- | --------- | ------------- | ------------------------ | ------------------------------------------------------------------------------------------------------------------------------ |
| **repository**    | `string`  | -             | `gitlab.com/project`     | URL for GitLab [Static Site Editor integration](https://docs.gitlab.com/ee/user/project/static_site_editor/).                  |
| **site_name**     | `string`  | -             | `Documentation Template` | Site label displayed next to logo in the navbar and also in the footer.                                                        |
| **last_updated**  | `boolean` | `true`        | `true`                   | Whether or not display last updated date under each documentation page. Date is obtained from _git_.                           |
| **sidebar_right** | `boolean` | `true`        | `true`                   | Whether or not display right sidebar navigation on each documentation page. It is composed from second level markdown headers. |
| **next_prev**     | `boolean` | `true`        | `true`                   | Whether or not display next and previous links under each documentation page.                                                  |

### navigation_links.json

`data/footer_links.json` file data from which navbar menu is generated. [Example](https://github.com/hajnyon/middleman-uikit-documentation-template/blob/master/template/data/navigation_links.json)

| property            | data type | default value | example value      | description                                                                                                                                 |
| ------------------- | --------- | ------------- | ------------------ | ------------------------------------------------------------------------------------------------------------------------------------------- |
| **url**             | `string`  | -             | `/docs/index.html` | URL to desired page. **Must be in absolute format!** See [#16](https://github.com/hajnyon/middleman-uikit-documentation-template/issues/16) |
| **label**           | `string`  | -             | `Docs`             | Displayed label of anchor.                                                                                                                  |
| **sub**             | `array`   | -             | -                  | An optional array of sub level links. One sub level allowed.                                                                                |
| **sub / # / url**   | `string`  | -             | `/docs/index.html` | URL to desired page. **Must be in absolute format!** See [#16](https://github.com/hajnyon/middleman-uikit-documentation-template/issues/16) |
| **sub / # / label** | `string`  | -             | `Docs`             | Displayed label of anchor.                                                                                                                  |

### footer_links.json

`data/footer_links.json` file contains an array of links displayed in the footer. Usually used for social links. [Example](https://github.com/hajnyon/middleman-uikit-documentation-template/blob/master/template/data/footer_links.json)

| property  | data type | default value | example value                 | description                                                              |
| --------- | --------- | ------------- | ----------------------------- | ------------------------------------------------------------------------ |
| **url**   | `string`  | -             | `https://twitter.com/hajnyon` | URL to desired site.                                                     |
| **label** | `string`  | -             | `Twitter`                     | Displayed label of anchor.                                               |
| **icon**  | `string`  | -             | `twitter`                     | Icon from [UIkit icons library](https://getuikit.com/docs/icon#library). |

## Frontmatter

You can configure few options per page using frontmatter. [Example](https://github.com/hajnyon/middleman-uikit-documentation-template/blob/master/template/source/docs/index.html.md)

| property       | data type | default value   | example value   | description                                                                                                                     |
| -------------- | --------- | --------------- | --------------- | ------------------------------------------------------------------------------------------------------------------------------- |
| **title**      | `string`  | -               | `Documentation` | Page title.                                                                                                                     |
| **layout**     | `string`  | `documentation` | `contents`      | One of the page [layouts](#layouts).                                                                                            |
| **order_type** | `string`  | -               | `alphabetical`  | Sorting of left sidebar (`documentation` layout) or content's list (`contents` layout). Unsorted if property left out or empty. |

## Layouts

Template supports 3 page layouts: documentation, contents and home. You can create custom layouts (see official [documentation](https://middlemanapp.com/basics/layouts/)) or change default layout for different folders of your project (see official [documentation](https://middlemanapp.com/basics/layouts/#custom-layouts)).

| name              | description                                                                                                                                                              |
| ----------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| **documentation** | Displays page content with optional sidebar, next and prev links and last updated date. Default layout for `source/docs` folder. [Example](./showcase.html)              |
| **contents**      | Displays page content followed by a list of pages in the same folder. [Example](./index.html)                                                                            |
| **home**          | Displays `hero` partial content (see `source/partials/_hero.erb`) followed by page content. Default layout for root file (`source/index.html`). [Example](../index.html) |  |

## Folder structure

This template uses the default middleman structure. If you want to learn more about it please follow official [documentation](https://middlemanapp.com/basics/directory-structure/) as in this article only basics are covered.

You can find short description of used files in the following schema:

```bash
my_middleman_site/
+-- Gemfile
+-- Gemfile.lock
+-- config.rb # various configuration such as default layouts and used markdown modules
+-- data # configuration files used by template - covered above
    +-- config.yml # basic template configuration - covered above
    +-- footer_links.json # footer links configuration - covered above
    +-- navigation_links.json # navbar links configuration - covered above
+-- helpers
    +-- custom_helpers.rb # helper function used by template
+-- source
    +-- docs # main folder for content
    +-- images # image assets used by template
    +-- javascripts # javascript assets (anchor, uikit)
        +-- site.js
        +-- ...
    +-- layouts # template's layouts - covered above
        +-- contents.erb
        +-- ...
    +-- partials  # partials used by template such as breadcrumbs, footer etc.
        +-- _breadcrumbs.erb
        +-- ...
    +-- stylesheets # styles (uikit, highlighting)
        +-- site.css.scss
        +-- ...
    +-- index.html.md # root file
```
