---
title: Showcase
order: 3
---

# Showcase

You can find source of this file in the [repository](https://gitlab.com/hajnyon/middleman-uikit-documentation-demo/-/blob/master/source/docs/showcase.html.md).

## Text

## Heading 2

### Heading 3

#### Heading 4

##### Heading 5

###### Heading 6

**bold text**

_italic text_

[link](//middlemanapp.com)

## Code

`inline code`

```ts
function blockCode(): string {
    return 'Hello world!';
}
```

## Table

| cell  | cell  | cell  |
| ----- | ----- | ----- |
| value | value | value |
| value | value | value |
| value | value | value |

## Lists

-   item
-   item
-   item

1. item
1. item
1. item

## Horizontal rule

---

## Image

![Cat yawning](images/cat.jpg "Cat yawning")

<div class="uk-text-right uk-text-small uk-text-light uk-margin-bottom">Photo by <a href="https://unsplash.com/@l_oan?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Loan</a> on <a href="https://unsplash.com/s/photos/cat?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Unsplash</a></div>
