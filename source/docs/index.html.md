---
title: Documentation
layout: contents
order_type: numeric
---

# Documentation

You will find how to install, update, use and configure this theme on the following pages. You can also check out how different types of content look on the showcase page.
