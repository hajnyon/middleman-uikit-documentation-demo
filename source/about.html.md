---
title: About
---

# About

This page was created for both demonstration and documentation of this Middleman template.

## Why?

Simply because there was no nice (subjectively) theme for documentation site in the Middleman. The author of this template was using mainly [VuePress](https://vuepress.vuejs.org/) for documentation of projects. However the need of the use of Middleman came once GitLab made their [Static Site Editor](https://docs.gitlab.com/ee/user/project/static_site_editor/) available and it supported (and supports) only Middleman SSG. There was no templates that would bring similar look as other SSGs used for documentation (VuePress, Docusaurus, GitBook etc.). And that is why this template was created.

## Who is it for?

Everyone who wants to create documentation pages using Middleman and possibly wants to use GitLab Static Site Editor.

## Where do I leave feedback?

If you have some bug reports, feature requests or just want to star this project, please use GitHub [repository](https://github.com/hajnyon/middleman-uikit-documentation-template).
