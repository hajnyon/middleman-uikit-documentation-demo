---
title: Documentation Template
---

<div class="uk-child-width-1-3@s uk-grid-match" uk-grid>
    <div class="uk-width-1-1 uk-text-center uk-flex uk-flex-center">
        <h2 class="no-anchor">Middleman is Running!</h2>
        <div class="uk-alert uk-alert-primary uk-width-1-2@m">
            <b>Middleman Docs Template</b> is a starting point for your documentation powered by Middleman and UIkit. It features simple & modern styling, automatic breadcrumbs & sidebar menu, code snippets highlight, list of contents and more.
            <a href="/middleman-uikit-documentation-demo/docs" class="uk-button uk-button-primary uk-margin-small">read the documentation</a>
        </div>
    </div>
    <div>
       <a href="/middleman-uikit-documentation-demo/docs/install" class="uk-display-block uk-card uk-card-body uk-card-default uk-link-toggle uk-text-center">
            <span uk-icon="icon: play; ratio:3"></span>
            <h3 class="no-anchor uk-card-title"><span class="uk-link-heading">Installation</span></h3>
            <p>Learn how to install and init your project with this template.</p>
        </a>
    </div>
    <div>
       <a href="/middleman-uikit-documentation-demo/docs/usage" class="uk-display-block uk-card uk-card-body uk-card-default uk-link-toggle uk-text-center">
            <span uk-icon="icon: code; ratio:3"></span>
            <h3 class="no-anchor uk-card-title"><span class="uk-link-heading">Usage</span></h3>
            <p>Overview of all supported features and options with examples.</p>
        </a>
    </div>
    <div>
       <a href="/middleman-uikit-documentation-demo/docs/showcase" class="uk-display-block uk-card uk-card-body uk-card-default uk-link-toggle uk-text-center">
            <span uk-icon="icon: album; ratio:3"></span>
            <h3 class="no-anchor uk-card-title"><span class="uk-link-heading">Showcase</span></h3>
            <p>Short page with styled markdown.</p>
        </a>
    </div>
</div>
